
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NewMap {

    public Map<String,List<String>> newMap( Map<String, String> inputMap){
        Map<String,List<String>> outputMap = new HashMap<>();
        for (Map.Entry<String,String> entry : inputMap.entrySet()){
            List<String> outputList = outputMap.get(entry.getValue());
            if (outputList == null){
                outputList = new ArrayList<>();
             }
            outputList.add(entry.getKey());
            outputMap.put(entry.getValue(), outputList);
        }
        return outputMap;


    }

}
